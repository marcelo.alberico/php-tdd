<?php

namespace App\Tests\Service;

use App\Model\Auction;
use App\Model\Bid;
use App\Model\User;
use App\Service\EvaluateService;

use PHPUnit\Framework\TestCase;

class EvaluateTest extends TestCase
{
    public function testShouldBeAbleToFindBiggerBidInAuction()
    {
        $auction = new Auction('Camisa Neymar');

        $zeze = new User('Zezé');
        $ale = new User('Alexandre');

        $auction->saveBid(new Bid($zeze, 2000));
        $auction->saveBid(new Bid($ale, 2500));

        $evaluate = new EvaluateService();

        $evaluate->check($auction);

        $biggerValue = $evaluate->getBiggerValue();

        self::assertEquals(2500, $biggerValue);
    }

    public function testShouldBeAbleToFindWinnerOfAuction()
    {
        $auction = new Auction('Camisa Neymar');

        $zeze = new User('Zezé');
        $ale = new User('Alexandre');

        $auction->saveBid(new Bid($zeze, 2000));
        $auction->saveBid(new Bid($ale, 2500));

        $evaluate = new EvaluateService();

        $evaluate->check($auction);

        $winner = $evaluate->getWinner();

        self::assertEquals($ale, $winner);
    }
}
