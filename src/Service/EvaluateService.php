<?php

namespace App\Service;

use App\Model\Auction;
use App\Model\User;

class EvaluateService
{
    private $biggerValue = -INF;
    private $smallerValue = INF;
    private $winner;

    public function check(Auction $auction): void
    {
        foreach ($auction->getBids() as $bid) {
            if ($bid->getValue() > $this->biggerValue) {
                $this->biggerValue = $bid->getValue();
                $this->winner = $bid->getUser();
            }

            if ($bid->getValue() < $this->smallerValue) {
                $this->smallerValue = $bid->getValue();
            }
        }
    }

    public function getBiggerValue(): float
    {
        return $this->biggerValue;
    }

    public function getSmallerValue(): float
    {
        return $this->smallerValue;
    }

    public function getWinner(): User 
    {
        return $this->winner;
    }
}
