<?php

namespace App\Model;

use App\Model\Bid;

class Auction
{
    private $bids;
    private $description;

    public function __construct(string $description)
    {
        $this->description = $description;
        $this->bids = [];
    }

    public function saveBid(Bid $bid)
    {
        $this->bids[] = $bid;
    }

    public function getBids(): array
    {
        return $this->bids;
    }
}
